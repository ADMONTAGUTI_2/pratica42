
import utfpr.ct.dainf.if62c.pratica.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author montaguti
 */
public class Pratica42 {
    public static void main(String[] args) {
        Circulo circulo = new Circulo(10);
        System.out.println("Perimetro Circulo: " + circulo.getPerimetro());
        System.out.println("Area Circulo: " + circulo.getArea());
        
        Elipse elipse = new Elipse(20, 20);
        System.out.println("Perimetro Elipse: " + elipse.getPerimetro());
        System.out.println("Area Elipse: " + elipse.getArea());
    }
}
